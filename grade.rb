
require_relative 'lib/student'
require_relative 'lib/grader'

# require the grader task to run and get the class
task_name = ARGV.shift
student_list = ARGV
unless task_name
  puts "Please specify task name. e.g. ruby grade.rb Lab1"
  exit
end

require_relative "task/#{task_name}"
grader_class = Object.const_get(task_name)
fail "Expect task/#{task_name}.rb to define class #{task_name}" if grader_class.nil?

# get all students
list = Student.all

# filter by student id
unless student_list.empty?
  list = list.select { |student| student_list.include?(student.student_id) }
end

# grade each student
list.each do |student|
  result = grader_class.new(student).run
  puts result
  puts ""
end

