import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.puppycrawl.tools.checkstyle.TreeWalker;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.FileContents;
import com.puppycrawl.tools.checkstyle.api.FileText;
import com.puppycrawl.tools.checkstyle.grammars.GeneratedJavaTokenTypes;


public class JavaAstXml {
	
	public static void main(String[] args) throws Exception {
				
		File file = new File(args[0]);
		List<String> lines = new ArrayList<String>();
		readLines(file, lines);
		FileText text = FileText.fromLines(file, lines);
		FileContents contents = new FileContents(text);
		DetailAST ast = TreeWalker.parse(contents);

		System.out.printf("<AST>\n");
		traverse(ast, 0);
		System.out.printf("</AST>\n");
		
	}	

	private static void traverse(DetailAST ast, int level) {
				
		visit(ast, level);
		DetailAST nextSibling = ast.getNextSibling();
		if (nextSibling != null) traverse(nextSibling, level);
		
	}

	private static void visit(DetailAST ast, int level) {
		int type = ast.getType();
		StringBuilder indent = new StringBuilder();
		for (int i = 0; i < level; i ++) indent.append("  ");
		String name = tokenTypeName(ast.getType());
		System.out.print(indent + "<" + name);
		System.out.printf(" line=\"%d\" column=\"%d\"", ast.getLineNo(), ast.getColumnNo());
		System.out.printf(" text=\"%s\"", escape(ast.getText()));
		
		DetailAST firstChild = ast.getFirstChild();
		if (firstChild != null) {
			System.out.printf(">\n");
			traverse(firstChild, level + 1);
			System.out.printf(indent + "</" + name + ">\n");
		} else {
			System.out.printf(" />\n");
		}
	}

	private static String escape(String text) {
		text = text.replaceAll("&", "&amp;");
		text = text.replaceAll("<", "&lt;");
		text = text.replaceAll(">", "&gt;");
		text = text.replaceAll("\"", "&quot;");
		return text;
	}

	private static Map<Integer, String> tokenMap = new HashMap<Integer, String>();
	
	static {
		try {
			for (Field field : GeneratedJavaTokenTypes.class.getDeclaredFields()) {
				tokenMap.put(field.getInt(null), field.getName());
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String tokenTypeName(int type) {
		return tokenMap.get(type);
	}

	private static void readLines(File file, List<String> lines) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		for (;;) {
			String line = reader.readLine();
			if (line == null) break;
			lines.add(line);
		}
	}

}
