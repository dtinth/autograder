#!/usr/bin/env ruby

require_relative 'lib/student'

class Student

  # Returns the SVN repository URL.
  def repo_url
    "https://se.cpe.ku.ac.th/psvn/#{@student_id}"
  end

  # Updates or checkouts the repository.
  def update_repo
    puts "#{@student_id}: "
    if File.exist?(repo_dir)
      system "./bin/my-svn update '#{repo_dir}'"
    else
      system "./bin/my-svn co '#{repo_url}' '#{repo_dir}'"
    end
  end

end

# update the repositories for every student
Student.all.each { |student| student.update_repo }

