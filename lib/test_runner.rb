
require 'shellwords'
require 'rexml/document'
require_relative 'grader_message'

class TestRunner

  # runs a test with options:
  #
  # options[:student][:dir]:: directory containing student's files
  # options[:student][:includes]:: space separated list of files in student's dir to compile
  # options[:test][:dir]:: directory containing test files
  # options[:test][:includes]:: space separated list of files in test dir to compile
  # options[:test][:name]:: qualified class name of the test class to run
  def self.run_test(options={})

    # construct the command
    args = []
    args << "-Dstudent.dir=#{File.realpath options[:student][:dir] or raise 'no :student :dir'}"
    args << "-Dstudent.includes=#{options[:student][:includes] or raise 'no :student :includes'}"
    args << "-Dtest.dir=#{File.realpath options[:test][:dir] or raise 'no :test :dir'}"
    args << "-Dtest.includes=#{options[:test][:includes] or raise 'no :test :includes'}"
    args << "-Dtest.name=#{options[:test][:name] or raise 'no :test :name'}"
    args.map! { |c| Shellwords.shellescape(c) }
    command = "ant test -f tests/build.xml #{args * ' '}"

    # run the command
    result = `#{command} 2>&1`

    if $? != 0

      # fail: match compile errors
      pattern = /^\s*\[javac\]\s?/
      message = result.lines.select { |line| line =~ pattern }.drop(1)
                      .map { |line| "  " + line.gsub(pattern, '').gsub(/\/\S+__(src|build)/, '/\1') }.join
      GraderMessage.new("Compile Error\n#{message}")

    else

      # grep the base directory. when error, show full result
      return GraderMessage.new("Cannot run test...\n#{result}") unless result =~ /!!!!base=([^!]+)!!!!/
      base = $1

      # find the result.xml file containing the unit test result
      result_file = File.join(base, 'result.xml')
      return GraderMessage.new("Cannot run test...\n#{result}") unless File.exist?(result_file)

      # read the xml result and create a report
      doc = REXML::Document.new(File.read(result_file))
      report = TestRunnerReport.new
      doc.elements.each('//testcase') do |testcase|
        report << testcase
      end
      doc.elements.each('//error') do |error|
        report.add_message("[ERROR] #{error.text}")
      end

      report

    end
  end
end

# a report for test runner
class TestRunnerReport

  def initialize
    @messages = []
  end

  # add a message to the report
  def add_message(message)
    @messages << message
  end

  # adds a testcase node to the report
  def <<(testcase)

    name = testcase.attributes['name']
    time = testcase.attributes['time']
    failures = testcase.get_elements('failure')

    # collect statistics
    @@stat ||= Hash.new { |h, k| h[k] = { :pass => 0, :fail => 0 } }

    # add messages
    if failures.empty?
      @messages << "[PASS] #{name}"
      @@stat[name][:pass] += 1
    else
      @messages << "[FAIL] #{name}"
      @@stat[name][:fail] += 1
      failures.each do |failure|
        @messages << "        - #{failure.attributes['message']}"
      end
    end

  end

  def write(out)
    @messages.each do |message|
      out << message << "\n"
    end
  end

  # return the test stat so far
  def self.stat
    @@stat
  end

end
