
# A student in this grader. Each student has own SVN repository.
class Student

  attr_reader :student_id

  # Returns a new Student with the specified _student_id_.
  def initialize(student_id)
    @student_id = student_id
  end

  # Returns the SVN repository directory.
  def repo_dir
    "psvn/#{@student_id}"
  end

  # Reads the student list and returns a list of created Student instances.
  def self.all
    File.readlines('student-list.txt').map(&:strip).reject(&:empty?).map { |id| Student.new(id) }
  end

end

