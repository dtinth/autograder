
require 'rexml/document'
require_relative 'java_file'

StyleError = Struct.new(:path, :line, :column, :message, :source, :file)

# A raw error returned from the checkstyle tool.
class StyleError
  def to_s
    " - %-40s %-30s %s" % [ "#{path}:#{line}", "#{source}", "#{message}" ]
  end
  def def_below
    file.def_below(line)
  end
  def def_above
    file.def_above(line)
  end
end

class REXML::Element
  # Returns the first element matching an xpath
  def get(xpath)
    elements.enum_for(:each, xpath).first
  end
end

# this class contains method for running the style checker
class StyleChecker

  # checks the code in the path, and returns a report
  def self.check(path)

    # make it real!
    path = File.realpath(path)

    # runs checkstyle
    command = "java -jar ./bin/checkstyle-5.6-all.jar -c ./bin/checkstyle.xml -r 'PATH' -f xml"
    doc = REXML::Document.new(`#{command.sub('PATH', path)}`)

    # read the resulting XML file
    errors = []
    doc.elements.each('//file') do |file_element|

      filename = file_element.attributes['name']
      realpath = File.realpath(filename)
      filepath = realpath[path.length..-1].lstrip.sub(/^\//, '')

      # skip file __SHELLXX.java
      next if File.basename(filepath) =~ /^__SHELL\d+\.java/i

      # skip file not matching standard conventions
      next unless File.basename(filepath) =~ /^([a-zA-Z0-9_]+)\.java/i

      # find the list of errors first.
      # don't do anything when there are no errors in this file...
      list = file_element.elements.enum_for(:each, 'error').to_a
      unless list.empty?
        file = JavaFile.new(realpath)
        list.each do |error_element|
          attrs = error_element.attributes
          line = attrs['line'].to_i
          column = attrs['column'].to_i
          message = attrs['message']
          source = attrs['source'].split('.').last
          errors << StyleError.new(filepath, line, column, message, source, file)
        end
      end

    end

    report = StyleCheckReport.new

    errors.each do |error|
      report << error
    end

    report

  end
end

# A class containing the style check result (report)
class StyleCheckReport

  @@all_stat = Hash.new(0)

  def initialize
    @missing_javadoc = []
    @javadoc_errors = []
    @mistakes = []
    @naming = []
    @advices = []
    @todos = []
    @stat = {}
  end

  def <<(error)
    source = error.source
    message = error.message
    case
    when source == 'JavadocMethodCheck' && message == 'Missing a Javadoc comment.'
      @missing_javadoc << "#{error.def_below}"
      record_stat :javadoc_missing
    when source == 'JavadocTypeCheck' && message == 'Missing a Javadoc comment.'
      @missing_javadoc << "#{error.def_below}"
      record_stat :javadoc_missing
    when source == 'JavadocMethodCheck' && message == 'Unused Javadoc tag.'
      @javadoc_errors << "#{error.def_below} contains unused Javadoc tag."
      record_stat :javadoc_error
    when source == 'JavadocTypeCheck' && message == 'Unused Javadoc tag.'
      @javadoc_errors << "#{error.def_below} contains unused Javadoc tag."
      record_stat :javadoc_error
    when source == 'JavadocMethodCheck' && message =~ /^Unused @param tag for \'([^']+)\'.$/
      @javadoc_errors << "#{error.def_below} does not have a param #{$1}, but found Javadoc @param tag."
      record_stat :javadoc_error
    when source == 'JavadocMethodCheck' && message =~ /^Expected @param tag for \'([^']+)\'.$/
      @javadoc_errors << "#{error.def_below}: @param #{$1} not found in Javadoc." unless error.def_below.name == 'main'
      record_stat :javadoc_error
    when source == 'JavadocMethodCheck' && message == 'Expected an @return tag.'
      @javadoc_errors << "#{error.def_below}: missing @return in Javadoc."
      record_stat :javadoc_error
    when source == 'StringLiteralEqualityCheck'
      @mistakes << "In #{error.def_above}, line #{error.line}, don't use == for comparing strings."
      record_stat :string_equal
    when source == 'SimplifyBooleanReturnCheck'
      @advices << "In #{error.def_above}, line #{error.line}, the if-else for return can be simplified."
      record_stat :boolean_return
    when source == 'MethodNameCheck' && message =~ /^Name '([^']+)'/
      @naming << "#{error.def_above}: #{advice_name_lower("method", $1)}"
      record_stat :method_name
    when source == 'LocalVariableNameCheck' && message =~ /^Name '([^']+)'/
      @naming << "#{error.def_above}: Line #{error.line}, #{advice_name_lower("variable", $1)}"
      record_stat :variable_name
    when source == 'TypeNameCheck' && message =~ /^Name '([^']+)'/
      @naming << "#{error.def_above}: #{advice_name_upper("type", $1)}"
      record_stat :type_name
    when source == 'TodoCommentCheck'
      @todos << { :path => error.path, :line => error.line }
      record_stat :todo
    when source == 'IndentationCheck'
      record_stat :indent
    else
      puts error
    end
  end

  def record_stat(type)
    @stat[type] = true
  end

  def commit_stats
    @stat.each do |key, collected|
      if collected
        @@all_stat[key] += 1
      end
    end
  end

  def advice_name_lower(type, name)
    suggest = name.sub(/^[^a-z]/) { |m| m.downcase }.gsub(/[^a-zA-Z0-9]+([a-z])/) { |m| $1.upcase }
    if name =~ /^[^a-z]/
      "#{type.capitalize} name should start with lowercase letter (e.g. #{suggest})."
    elsif name =~ /_/
      "#{type.capitalize} name should not contain underscores (e.g. #{suggest})."
    else
      "#{type.capitalize} name should use only alphanumeric characters (e.g. #{suggest})."
    end
  end

  def advice_name_upper(type, name)
    suggest = name.sub(/^[^A-Z]/) { |m| m.upcase }.gsub(/[^a-zA-Z0-9]+([a-z])/) { |m| $1.upcase }
    if name =~ /^[^A-Z]/
      "#{type.capitalize} name should start with uppercase letter (e.g. #{suggest})."
    elsif name =~ /_/
      "#{type.capitalize} name should not contain underscores (e.g. #{suggest})."
    else
      "#{type.capitalize} name should use only alphanumeric characters (e.g. #{suggest})."
    end
  end

  def write(out)
    @written = false
    write_section out, "TODO comment leftovers:", @todos.group_by { |x| x[:path] }.map { |path, errs| "#{path} line#{errs.length > 1 ? "s" : ""} #{errs.map { |x| x[:line] }.join(', ')}" }
    write_section out, "These classes/methods/constructors are missing Javadoc comment:", @missing_javadoc
    write_section out, "#{!@missing_javadoc.empty? ? "Other " : ""}Javadoc errors:", @javadoc_errors
    write_section out, "Naming conventions:", @naming
    write_section out, "#{@written ? "Other " : ""}Advices:", (@mistakes + @advices)
    unless @written
      out << "Passed the style check without any messages.\n"
    end
    commit_stats
  end

  def write_section(out, header, list)
    if !list.empty?
      out << "#{header}\n"
      list.each do |item|
        out << " - #{item}\n"
      end
      @written = true
    end
  end

end

