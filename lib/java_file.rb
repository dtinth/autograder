
require 'rexml/document'

# A Java source file with parsed AST.
class JavaFile

  attr_accessor :filename

  # reads and parses the java file specified at \filename for outlines
  def initialize(filename)

    @filename = filename
    @contents = []

    # Generate the AST of the Java source file.
    command = "java -cp ./bin/checkstyle-5.6-all.jar:./bin/ JavaAstXml 'PATH'"
    doc = REXML::Document.new(`#{command.sub('PATH', filename)}`)

    walk(doc, '/AST/CLASS_DEF', :walk_class_def, JavaClass, nil)
    @contents.sort_by!(&:line)

  end

  # find a definition on or below the specified line
  def def_below(line)
    @contents.drop_while { |node| node.line < line }.first
  end

  # find a definition on or above the specified line
  def def_above(line)
    @contents.take_while { |node| node.line <= line }.last
  end

  private

  # ---------------- ast tree walker ----------------
  
  # utility method for traversing dom and creating outline nodes
  def walk(dom, xpath, method, klass, parent)
    dom.elements.each(xpath) do |node|
      simple_ast = klass.new(node.get('IDENT').attributes['text'], parent, node)
      @contents << simple_ast
      send method, simple_ast
    end
  end

  def walk_class_def(java_class)
    node = java_class.node
    walk(node, 'OBJBLOCK/VARIABLE_DEF', :walk_variable_def, JavaField, java_class)
    walk(node, 'OBJBLOCK/CTOR_DEF', :walk_ctor_def, JavaConstructor, java_class)
    walk(node, 'OBJBLOCK/METHOD_DEF', :walk_method_def, JavaMethod, java_class)
  end

  def walk_variable_def(java_field)
  end

  def walk_ctor_def(java_constructor)
  end

  def walk_method_def(java_method)
  end



  # ---------------- ast nodes -----------------

  # Java source outline node.
  class JavaNode
    attr_reader :name, :parent, :node, :line

    def initialize(name, parent, node)
      @name = name
      @parent = parent
      @node = node
      @line = node.attributes['line'].to_i
      set_up
    end

    # set up the instance (read additional values from dom node), to be overridden
    def set_up
    end

    # return the parent string
    def parent_s
      @parent.nil? ? "" : @parent.pathname + "."
    end

    def to_s
      "#{pathname}"
    end

    def basename
      name
    end

    def pathname
      parent_s + basename
    end

  end

  class JavaClass < JavaNode

    def to_s
      "class #{super}"
    end

  end

  class JavaField < JavaNode
  end

  class JavaCallable < JavaNode

    def set_up
      @params = []
      @node.get('PARAMETERS').elements.each('PARAMETER_DEF') do |parameter_def|
        @params << parameter_def.get('IDENT').attributes['text']
      end
    end

    def basename
      "#{@name}(#{@params.join(', ')})"
    end

  end

  class JavaMethod < JavaCallable
  end

  class JavaConstructor < JavaCallable
  end


end
