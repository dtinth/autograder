
require_relative 'style_checker'
require_relative 'grader_message'
require_relative 'test_runner'

# a project grader, capable of doing automated style checking and JUnit testing
class Grader

  # class helper to set the directory match regular expression.
  def self.match_directory(regexp)
    @matcher = regexp
  end

  # checks if the given directory matches this class's directory match regular expression or not.
  def self.check_directory(dirname)
    dirname =~ @matcher
  end

  # initialize a grader for a student project
  def initialize(student)
    @student = student
    @result = GradeResult.new(student)
    directory = Dir[File.join(@student.repo_dir, '*')].map { |path| File.basename(path) }.find do |basename|
      self.class.check_directory(basename)
    end
    @path = nil
    begin
      @path = File.realpath(File.join(@student.repo_dir, directory)) unless directory.nil?
    rescue Errno::ENOENT
    end
    @result.path = @path
  end

  # grades the student
  def run
    if @path.nil?
      emit_message "Cannot find the project. Did you name it properly?"
    else
      grade
    end
    @result
  end

  # run grader (override me)
  def grade
    check_style
  end

  # performs a style checking
  def check_style
    @result << GraderResultSection.new("Style Check", StyleChecker.check(@path))
  end

  # runs a unit test
  def unit_test(student_files, test_name, test_files, test_class)
    result = TestRunner.run_test(:student => { dir: @path, includes: student_files },
                                 :test => { dir: "tests/#{test_name}", includes: test_files, name: test_class })
    @result << GraderResultSection.new("Unit Test: #{test_name}", result)
  end

  # emits some message
  def emit_message(message)
    @result << GraderMessage.new(message)
  end

end

# just a result section to make it look nicer
class GraderResultSection
  def initialize(name, result)
    @name = name
    @result = result
  end
  def write(out)
    out << "== #{@name} ==\n"
    buffer = ""
    @result.write buffer
    out << buffer.lines.map { |line| "  #{line}" }.join
  end
end

# the result of grading
class GradeResult

  attr_reader :student
  attr_writer :path

  def initialize(student)
    @student = student
    @reports = []
  end

  # adds a report to the result
  def <<(report)
    @reports << report
  end

  def to_s
    out = ""
    write out
    out
  end

  # write the result into `out`
  def write(out)
    out << "[ #{@student.student_id} ]"
    if @path
      path = @path[File.realpath(@student.repo_dir).length..-1]
      out << " @ #{path}"
    end
    out << "\n"
    @reports.each do |report|
      report.write out
      out << "\n"
    end
  end

end


