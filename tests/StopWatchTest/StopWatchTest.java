import static org.junit.Assert.*;

import org.junit.Test;


public class StopWatchTest {
	
	private StopWatch sw = new StopWatch();
	
	@Test
	public void testStartDoesNothingIfAlreadyRunning() {
		sw.start();
		sleep(200);
		sw.start();
		sleep(200);
		sw.stop();
		assertEquals(0.4, sw.getElapsed(), 1e-2); // error: 0.2
	}

	@Test
	public void testStopDoesNothingIfNotRunning() {
		sw.start();
		sleep(200);
		sw.stop();
		sleep(200);
		sw.stop();
		assertEquals(0.2, sw.getElapsed(), 1e-2); // error: 0.4
	}

	
//
//	@Test
//	public void testNotRunningBeforeStart() {
//		assertEquals(false, sw.isRunning());
//	}
//
//	@Test
//	public void testRunningAfterStart() {
//		sw.start();
//		assertEquals(true, sw.isRunning());
//	}
//	
//	@Test
//	public void testNotRunningAfterStop() {
//		sw.start();
//		sw.stop();
//		assertEquals(false, sw.isRunning());
//	}
//
//	@Test
//	public void testElapsedBeforeStart() {
//		assertEquals(0.0, sw.getElapsed(), 1e-2);
//		sleep(100);
//		assertEquals(0.0, sw.getElapsed(), 1e-2);
//	}
//
//	@Test
//	public void testElapsedAfterStart() {
//		sw.start();
//		assertEquals(0.0, sw.getElapsed(), 1e-2);
//		sleep(200);
//		assertEquals(0.2, sw.getElapsed(), 1e-1);
//		sleep(200);
//		assertEquals(0.4, sw.getElapsed(), 1e-1);
//	}
//
//	@Test
//	public void testElapsedAfterStop() {
//		sw.start();
//		sleep(200);
//		sw.stop();
//		assertEquals(0.2, sw.getElapsed(), 1e-1);
//		sleep(200);
//		assertEquals(0.2, sw.getElapsed(), 1e-1);
//	}
//	
	private void sleep(long howLong) {
		try {
			Thread.sleep(howLong);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
}
