import java.util.Random; 
/**
 * Guess a secret number between 1 and an upper bound.
 * 
 * @author Your Name
 * @version year,month,day
 */
public class Game {

    /** upper bound for secret number */
    private int upperBound;
    /** the secret number */
    private int secret;
    /** hint from the most recent guess */
    private String hint;
	/** count guesses */
    private int count = 0;

    /** Initialize a new default game. */
    public Game()
    {
    	this(100);
    }

    public Game(int bound) {
		this.upperBound = bound;
		secret = getRandomNumber(upperBound);
		hint = "I'm thinking of a number between 1 and "+upperBound;
	}

	/** 
     * Get a random number between 1 and limit.
     * @param limit is the upper limit for the random number.
     * @return a random integer
     */
    private int getRandomNumber(int limit) {
        // use a seed so the random numbers are not repeatable
        long seed = System.nanoTime( );
		Random random = new Random(seed);
		return random.nextInt( limit ) + 1;
    }
   
    /**
     * Evaluate a guess. 
     * @param number is the user's guess
     * @return true if guess correct, false otherwise
     */
    public boolean guess(int number) {
		hint = makeHint(number, secret);
		return secret == number;
    }
    
	public String getHint() {
		return hint;
	}
	
	/** Get the game upper bound. */
	public int getUpperBound() {
		return upperBound;
	}
    
    /**
     * Create a hint based on the last guess.
     * @param guess is the most recent guessed number
     * @param secret is the secret number
     */
    protected String makeHint(int guess, int secret) {
        if (guess == secret) return "You got it.";
        else if (guess < secret) return "Sorry, too small.";
        else return "Sorry, too large.";
    }
}