import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;


public class GameTest {

	private boolean badSecret = false;
	private int badSecretExample;
	private int badSecretBound;
	private boolean badHint = false;
	private boolean liar = false;
	
	public boolean hasDefaultConstructor() {
		try {
			Game.class.getConstructor();
			return true;
		} catch (NoSuchMethodException | SecurityException e) {
			return false;
		}
	}
	
	public boolean hasIntConstructor() {
		try {
			Game.class.getConstructor(Integer.TYPE);
			return true;
		} catch (NoSuchMethodException | SecurityException e) {
			return false;
		}
	}
	
	public Game createGame(int bound) {
		try {
			if (bound == 0 && !hasDefaultConstructor()) {
				return Game.class.getConstructor(Integer.TYPE).newInstance(100);
			} else if (hasIntConstructor() && bound != 0) {
				return Game.class.getConstructor(Integer.TYPE).newInstance(bound);
			} else {
				return Game.class.getConstructor().newInstance();
			}
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			fail("Cannot create game object: " + e.getMessage() + " cause: " + e.getCause());
			return null;
		}
	}

	@Test
	public void testDefaultConstructor() {
		if (!hasDefaultConstructor()) fail("No default constructor.");
	}

	@Test
	public void testIntConstructor() {
		if (!hasIntConstructor()) fail("No int constructor.");
	}
	
	@Test(timeout=1000)
	public void testUpperBound() {
		if (!hasIntConstructor()) fail("Cannot test because there is no constructor that can set upper bound.");
		int bound = createGame(55555).getUpperBound();
		if (bound != 55555) {
			fail("Upperbound returns wrong value. Expected 55555 but got " + bound);
		}
	}
	
	@Test(timeout=1000)
	public void testSecret() {
		doTest();
		if (badSecret) fail("Game generates secret (" + badSecretExample + ") outside of range [1, " + badSecretBound + "]");
	}
	
	@Test(timeout=1000)
	public void testHintText() {
		doTest();
		if (badHint) fail("Game generates invalid hints (neither 'too large' nor 'too small')");
	}
	
	@Test(timeout=1000)
	public void testHintLie() {
		doTest();
		if (liar) fail("Game lied in its hints, thus impossible to solve.");
	}
	
	private void doTest() {
		for (int i = 0; i < 3000; i ++) {
			testIteration(i);
		}
	}

	public void testIteration(int i) {
		Game game = createGame(i);
		int bound = game.getUpperBound();
		if (bound == 0) bound = i;
		if (bound == 0) bound = 100;
		long min = Integer.MIN_VALUE;
		long max = Integer.MAX_VALUE;
		while (min <= max) {
			long mid = (min + max) / 2;
			boolean result = game.guess((int)mid);
			if (result) {
				if (mid < 1 || mid > bound) {
					if (!badSecret) {
						badSecret = true;
						badSecretExample = (int)mid;
						badSecretBound = bound;
					}
				}
				return;
			}
			String hint = game.getHint().toLowerCase();
			if (hint.contains("small")) {
				min = mid + 1;
			} else if (hint.contains("large") || hint.contains("big")) {
				max = mid - 1;
			} else {
				badHint = true;
				return;
			}
		}
		liar = true;
	}

}
