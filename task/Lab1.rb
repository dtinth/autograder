
class Lab1 < Grader

  match_directory /^(?:Term2|Lab01|Lap01|LAB1|lab_1|Lab1GuessingGame|GuessingGame|GussingGame|Guessing Game|GuessingGameBlueJ|project1|fgg|sdjk|firstproject|JavaLab16-1-56|test|LabKOK)$/i

  def grade
    check_style
    unit_test 'Game.java', 'GuessingGameTest', 'GameTest.java', 'GameTest'
  end

end
