
class PA1 < Grader

  match_directory /^PA\s*1$/i

  def grade
    check_style
  end

end
