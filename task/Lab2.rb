
class Lab2 < Grader

  match_directory /^(?:Lab02|Lap02)$/i

  def grade
    check_style
    unit_test 'StopWatch.java', 'StopWatchTest', 'StopWatchTest.java', 'StopWatchTest'
  end

end
